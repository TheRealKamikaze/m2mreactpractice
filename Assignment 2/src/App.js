import React from 'react';
import MemberDetails from './MemberDetails'
import 'bootstrap/dist/css/bootstrap.css'
class App extends React.Component{
  constructor(){
    super();
    this.state = {
      isDataPopulated: false,
      ids: [],
      isDataFetched: false,
      selectedId: -1
    }
    this.handleClick = this.handleClick.bind(this)
  }
  
  async componentDidMount(){
    try{
      console.log("Mounted!!")
      let fetchIds = await fetch('https://yayjk.dev/api/members/allIds').then(result => result.json());
      await this.setState({
        ids: fetchIds.memberIdsList,
        isDataFetched: true
      })
    console.log(this.state.ids);
  }catch(err){
    console.log(err)
  }
  }

  renderIds(){
    return (
      <div>
        <ul className="list-group list-group-flush">
        {this.state.ids.map((id,i)=><li className="list-group-item list-group-item-action" key = {id._id} value = {i}  onClick={this.handleClick}>{id._id}</li>)}      
        </ul>
      </div>
    )
  }

  handleClick(e){
    this.setState({
      isDataPopulated: true,
      selectedId: this.state.ids[e.target.value]._id
    })
  }
  
  render(){
    console.log("rendered");
      if(!this.state.isDataPopulated){
        return (
          <div className = "container justify-content-md-center">
            <h1>List of members are as follows: </h1>
            {!this.state.isDataFetched? <div>loading...</div>: 
            <div>
              {this.renderIds()}
            </div> 
            }         
          </div>
        ) 
      }else{
        return (
          <div>
            <MemberDetails id= {this.state.selectedId} />
          </div>
        )
      }
    }
}

export default App;
