import React from 'react'

class MemberDetails extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            isDataFetched: false,
            selectedId: this.props.id,
            member: {}
        }
        this.handleClick = this.handleClick.bind(this);
    }
    
    async componentDidMount(){
        let result = await fetch('https://yayjk.dev/api/members/'+this.props.id);
        result = await result.json();
        let memberDetails = result.memberDetails;
        memberDetails.teamMembersId = result.teamMembersId;
        this.setState({
            isDataFetched: true,
            member: memberDetails
        })
        console.log("mounted!!", this.state.member);
    }

    async componentDidUpdate(prevProps, prevState){
        if(prevState.selectedId!== this.state.selectedId){
            let result = await fetch('https://yayjk.dev/api/members/'+this.state.selectedId);
            result = await result.json();
            let memberDetails = result.memberDetails;
            memberDetails.teamMembersId = result.teamMembersId;
            this.setState({
                isDataFetched: true,
                member: memberDetails
            })
            console.log("mounted!!", this.state.member);
        }
    }
    
    async handleClick(e){
        await this.setState({
          selectedId: this.state.member.teamMembersId[e.target.value]._id
        })
        // console.log(this.state);
      }

    generateMember(){
        return (
            <div className ="container">
                <div className = "row">
                    <div className="col-lg-4">
                        <h3>Team Members: </h3>
                        <ul className="list-group list-group-flush">
                            {this.state.member.teamMembersId.map((id,i)=><li className="list-group-item list-group-item-action" key = {id._id} value = {i}  onClick={this.handleClick}>{id._id}</li>)}     
                        </ul>
                    </div>
                    <div className= "col-lg-8">
                        <h3>MemberDetails</h3>
                        <table className = "table">
                            <tbody>
                                <tr>
                                <th scope= "column">Name: </th>
                                <th scope= "column">email: </th>
                                <th scope= "column">Position: </th>
                                </tr>
                                <tr>
                                    <td>{this.state.member.name} </td>
                                    <td>{this.state.member.email} </td>
                                    <td>{this.state.member.position} </td>
                                </tr>
                                <tr>
                                <th scope= "column">team: </th>
                                <th scope= "column">Phone: </th>
                                <th scope= "column">Date of Joining!: </th>
                                </tr>
                                <tr>
                                    <td>{this.state.member.team} </td>
                                    <td>{this.state.member.phone} </td>
                                    <td>{this.state.member.dateOfJoining} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
    render(){
    return (
        <div>
            {!this.state.isDataFetched? <h1>Hello, member</h1>: this.generateMember()}
        </div>
        
    )
    }
}

export default MemberDetails