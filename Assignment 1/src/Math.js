import React from 'react';

class Math1 extends React.Component{
  constructor(props){
    super(props);
  }
  
  calculateSolution(){
    console.log(this.props.operation)
    switch(Number(this.props.operation)){
      case 1: console.log("Here"); 
      return Math.pow(Number(this.props.a)+Number(this.props.b),2);
      case 2: let determinant = Number(this.props.b)*Number(this.props.b)-4*Number(this.props.a)*Number(this.props.c); 
      if(determinant<0){
        return "No real solution"
      }else{
        return `${(-Number(this.props.b)+Math.sqrt(determinant))/(2*Number(this.props.a))}  ${(-Number(this.props.b)-Math.sqrt(determinant))/(2*Number(this.props.a))}`
      }
      case 3: return Math.pow(Number(this.props.a),1/Number(this.props.b));
      case 4: let fact = 1;
              for(let i=1; i<=Number(this.props.a); i++){
                fact*=i;
              }
              return fact;
      case 5: return `Tan: ${Math.tan(Number(this.props.a))}, sin: ${Math.sin(Number(this.props.a))}, cos: ${Math.cos(Number(this.props.a))}`;
              
    }
  }
  render(){
    return (
      <div>
        {`Solution is: ${this.calculateSolution()}`}
      </div>
    )
  }
}

export default Math1
