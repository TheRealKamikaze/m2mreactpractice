import React from "react";
import ReactDOM from "react-dom";
import Math1 from "./Math"
class App extends React.Component{
  constructor(){
    super();
    this.state = {
      operation: 0,
      clicked: false, 
      a: undefined,
      b: undefined,
      c: undefined
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }
  
  async handleChange(e){
    await this.setState({operation: e.target.value});
    document.getElementById('a').value = ""
    document.getElementById('b').value= "" 
    document.getElementById('c').value= ""
  }
  
  async handleClick(){
    console.log("here");
    await this.setState({
      a: document.getElementById('a').value, 
      b: document.getElementById('b').value, 
      c: document.getElementById('c').value,
      clicked: true
      })
  }

  render(){
    return (
      <div>
        <select onChange={this.handleChange}>
          <option value={0}>Select an operation </option>
          <option value={1}>(a+b)^2 </option>
          <option value={2}>Quadratic Equation </option>
          <option value={3}> Nth Root of a number </option>
          <option value={4}>Factorial of Number </option>
          <option value={5}>Trignometric values(sin, cos, tan) </option>
        </select>
        <div style={{ display: Number(this.state.operation) ? "block" : "none" }}>
          <input id = "a" type = "text" placeholder = "a/number"/>
          <br />
          <input style={{ display: Number(this.state.operation)===1 || Number(this.state.operation)===3 || Number(this.state.operation)===2 ? "block" : "none" }} id = "b" type = "text" placeholder = "b/n"/>
          <br />
          <input style={{ display: Number(this.state.operation)===2 ? "block" : "none" }} id = "c" type = "text" placeholder = "c"/>
          <br />
          <button onClick = {this.handleClick}>Submit</button>
        </div>
        { this.state.clicked && <Math1 operation= {this.state.operation} a={this.state.a}  b={this.state.b} c={this.state.c} />}
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById("root"));